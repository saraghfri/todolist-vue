import {defineStore} from 'pinia'
import {computed, ref} from 'vue'

export const useTodoStore = defineStore('todo', () => {
    const todos = ref(JSON.parse(localStorage.getItem('todos')) || [])

    const createModalVisible = ref(false);
    const modalTitle = ref('Add Task');
    const editTodoObj = ref(null);

    const getCreateModalVisible = computed(() => {
        return createModalVisible.value
    })

    const getModalTitle = computed(() => {
        return modalTitle.value
    })

    const getEditTodoObj = computed(() => {
        return editTodoObj.value
    })

    const closeCreateModal = () => {
        createModalVisible.value = false
    }
    const openModifyModal = (todo = null) => {
        editTodoObj.value = todo
        createModalVisible.value = true
        modalTitle.value = todo ? 'Edit Task' : 'Add Task'
    }
    const addTodo = (todo) => {
        todos.value.push(todo)
        syncLocalStorage()
    }

    const editTodo = (id, updatedTodo) => {
        const index = todos.value.findIndex(todo => todo.id === id)
        if (index !== -1) {
            todos.value[index] = {...todos.value[index], ...updatedTodo}
            syncLocalStorage()
        }
    }

    const deleteTodo = (id) => {
        todos.value = todos.value.filter(todo => todo.id !== id)
        syncLocalStorage()
    }

    const syncLocalStorage = () => {
        localStorage.setItem('todos', JSON.stringify(todos.value))
        window.dispatchEvent(new Event('storage'))
    }

    const filterTodos = (status) => {
        return status ? todos.value.filter(todo => todo.status === status) : todos.value
    }

    const searchTodos = (query) => {
        return query ? todos.value.filter(todo => todo.title.includes(query)) : todos.value
    }

    window.addEventListener('storage', () => {
        todos.value = JSON.parse(localStorage.getItem('todos')) || []
    })

    return {
        todos,
        addTodo,
        editTodo,
        deleteTodo,
        filterTodos,
        searchTodos,
        closeCreateModal,
        openModifyModal,
        getCreateModalVisible,
        getModalTitle,
        getEditTodoObj
    }
})